import java.math.BigDecimal;

import static java.lang.Long.parseLong;
import static java.math.RoundingMode.HALF_EVEN;
import static java.math.RoundingMode.HALF_UP;
import static java.util.Objects.hash;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {
  private final long numerator;
  private final long denominator;

  /**
   * Main method. Different tests.
   */
  public static void main(String[] param) {
    Lfraction frac = new Lfraction(3, -13);
  }

  /**
   * Constructor.
   *
   * @param a numerator
   * @param b denominator > 0
   */
  public Lfraction(long a, long b) {
    if (b == 0) {
      throw new IllegalArgumentException("Denominator cannot be zero! Denominator was: " + b);
    }

    long gcd = gcdByEuclidsAlgorithm(a, b);
    long reducedNumer = a / gcd;
    long reducedDenom = b / gcd;

    this.numerator = reducedDenom < 0 ? -reducedNumer : reducedNumer;
    this.denominator = reducedDenom < 0 ? -reducedDenom : reducedDenom;
  }

  /**
   * Copy Constructor.
   *
   * @param fraction fraction to be copied
   */

  private Lfraction(Lfraction fraction) {
    this.numerator = fraction.getNumerator();
    this.denominator = fraction.getDenominator();
  }

  /**
   * Public method to access the numerator field.
   *
   * @return numerator
   */
  public long getNumerator() {
    return this.numerator;
  }

  /**
   * Public method to access the denominator field.
   *
   * @return denominator
   */
  public long getDenominator() {
    return this.denominator;
  }

  /**
   * Conversion to string.
   *
   * @return string representation of the fraction
   */
  @Override
  public String toString() {
    return numerator + "/" + denominator;
  }

  /**
   * Equality test.
   *
   * @param m second fraction
   * @return true if fractions this and m are equal
   */
  @Override
  public boolean equals(Object m) {
    if (!(m instanceof Lfraction)) {
      return false;
    }

    return compareTo((Lfraction) m) == 0;
  }

  /**
   * Hashcode has to be equal for equal fractions.
   *
   * @return hashcode
   */
  @Override
  public int hashCode() {
    Lfraction fraction = createReducedFraction(numerator, denominator);
    return hash(fraction.getNumerator(), fraction.getDenominator());
  }

  /**
   * Sum of fractions.
   *
   * @param m second addend
   * @return this+m
   */
  public Lfraction plus(Lfraction m) {
    long resultNumerator = numerator * m.getDenominator() + m.getNumerator() * denominator;
    long resultDenominator = denominator * m.getDenominator();

    return createReducedFraction(resultNumerator, resultDenominator);
  }

  /**
   * Multiplication of fractions.
   *
   * @param m second factor
   * @return this*m
   */
  public Lfraction times(Lfraction m) {
    long resultNumerator = numerator * m.getNumerator();
    long resultDenominator = denominator * m.getDenominator();

    return createReducedFraction(resultNumerator, resultDenominator);
  }

  /**
   * Inverse of the fraction. n/d becomes d/n.
   *
   * @return inverse of this fraction: 1/this
   */
  public Lfraction inverse() {
    if (numerator == 0) {
      throw new IllegalArgumentException("Numerator cannot be zero! Numerator was: " + numerator);
    }

    return new Lfraction(denominator, numerator);
  }

  /**
   * Opposite of the fraction. n/d becomes -n/d.
   *
   * @return opposite of this fraction: -this
   */
  public Lfraction opposite() {
    return new Lfraction(-numerator, denominator);
  }

  /**
   * Difference of fractions.
   *
   * @param m subtrahend
   * @return this-m
   */
  public Lfraction minus(Lfraction m) {
    return this.plus(m.opposite());
  }

  /**
   * Quotient of fractions.
   *
   * @param m divisor
   * @return this/m
   */
  public Lfraction divideBy(Lfraction m) {
    if (m.getNumerator() == 0) {
      throw new IllegalArgumentException("Numerator of input fraction cannot be 0. Input was: " + m);
    }

    return this.times(m.inverse());
  }

  /**
   * Comparison of fractions.
   *
   * @param m second fraction
   * @return -1 if this < m; 0 if this==m; 1 if this > m
   */
  @Override
  public int compareTo(Lfraction m) {
    BigDecimal thisNumDecimal = new BigDecimal(Long.toString(numerator));
    BigDecimal thisDenomDecimal = new BigDecimal(Long.toString(denominator));

    BigDecimal incomingNumDecimal = new BigDecimal(Long.toString(m.getNumerator()));
    BigDecimal incomingDenomDecimal = new BigDecimal(Long.toString(m.getDenominator()));

    BigDecimal firstResult = thisNumDecimal.divide(thisDenomDecimal, 10, HALF_EVEN);
    BigDecimal secondResult = incomingNumDecimal.divide(incomingDenomDecimal, 10, HALF_EVEN);

    return firstResult.compareTo(secondResult);
  }

  /**
   * Clone of the fraction.
   *
   * @return new fraction equal to this
   */
  @Override
  public Object clone() throws CloneNotSupportedException {
    return new Lfraction(this);
  }

  /**
   * Integer part of the (improper) fraction.
   *
   * @return integer part of this fraction
   */
  public long integerPart() {
    return BigDecimal
      .valueOf(this.toDouble())
      .longValue();
  }

  /**
   * Extract fraction part of the (improper) fraction
   * (a proper fraction without the integer part).
   *
   * @return fraction part of this fraction
   */
  public Lfraction fractionPart() {
    return new Lfraction(numerator % denominator, denominator);
  }

  /**
   * Approximate value of the fraction.
   *
   * @return numeric value of this fraction
   */
  public double toDouble() {
    BigDecimal newNumDecimal = new BigDecimal(Long.toString(numerator));
    BigDecimal newDenomDecimal = new BigDecimal(Long.toString(denominator));

    return (newNumDecimal.divide(newDenomDecimal, 1000, HALF_EVEN)).doubleValue();
  }

  /**
   * Double value f presented as a fraction with denominator d > 0.
   *
   * @param f real number
   * @param d positive denominator for the result
   * @return f as an approximate fraction of form n/d
   */
  public static Lfraction toLfraction(double f, long d) {
    long numeratorResult = BigDecimal
      .valueOf(f)
      .multiply(BigDecimal.valueOf(d))
      .setScale(0, HALF_UP)
      .longValue();

    return new Lfraction(numeratorResult, d);
  }

  /**
   * Conversion from string to the fraction. Accepts strings of form
   * that is defined by the toString method.
   *
   * @param s string form (as produced by toString) of the fraction
   * @return fraction represented by s
   */
  public static Lfraction valueOf(String s) {
    String[] splitString = s.split("\\/");

    for (String splitNumber : splitString) {
      if (!isNumeric(splitNumber)) {
        throw new IllegalArgumentException("Input contains non-numeric argument! Input was: " + s);
      }

      if (splitString.length != 2) {
        throw new IllegalArgumentException("Input contains too many arguments! Input was: " + s);
      }

      if (!isParsableLong(splitNumber)) {
        throw new IllegalArgumentException("Input cannot be converted to long! Input was: " + s);
      }
    }

    return new Lfraction(parseLong(splitString[0]), parseLong(splitString[1]));
  }

  /**
   * Finds the highest common denominator.
   * source: https://www.baeldung.com/java-greatest-common-divisor
   *
   * @param numer   first addend
   * @param denomin second addend
   * @return highest common denominator long
   */
  public static long gcdByEuclidsAlgorithm(long numer, long denomin) {
    if (denomin == 0) {
      return numer;
    }
    return gcdByEuclidsAlgorithm(denomin, numer % denomin);
  }

  /**
   * Creates fraction reduced to its greatest common denominator;
   *
   * @param resultNumerator   long.
   * @param resultDenominator long.
   * @return reduced fraction Lfraction.
   */
  private Lfraction createReducedFraction(long resultNumerator, long resultDenominator) {
    long gcd = gcdByEuclidsAlgorithm(resultNumerator, resultDenominator);
    return new Lfraction(resultNumerator / gcd, resultDenominator / gcd);
  }

  /**
   * Determines if string is numeric.
   *
   * @param argument string to be processed.
   * @return boolean is numeric.
   */
  private static boolean isNumeric(String argument) {
    try {
      parseLong(argument);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Determines if string can be parsed to long.
   *
   * @param argument string to be processed.
   * @return boolean can be converted to long.
   */
  private static boolean isParsableLong(String argument) {
    try {
      parseLong(argument);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }
  }

  public Lfraction pow(long n) {
    Lfraction res = new Lfraction(numerator, denominator);

    if (n > 0) {
      if (n == 1) {
        return res;
      }
      return res.times(res.pow(n - 1));
    } else if (n < 0) {
      if (n == -1) {
        return res.inverse();
      } else {
        return res.pow(n * (-1)).inverse();
      }
    } else {
      return new Lfraction(1, 1);
    }
  }
}
